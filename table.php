<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Table</title>
</head>
<body>

<table border="1" align="center">
    <h3 align="center">Book Title - List</h3>
    <tr>
        <th>Serial</th>
        <th>ID</th>
        <th>Book Title</th>
        <th>Author</th>
        <th colspan="6" align="center">Action</th>
    </tr>
    <tr>
        <th>1</th>
        <th>5</th>
        <th>PHP-BOOK TITLE #1</th>
        <th>AUTHOR #1</th>
        <td><input type="button"value="view"></td>
        <td><input type="button"value="edit"></td>
        <td><input type="button"value="delete"></td>
        <td><input type="button"value="trash"></td>
    </tr>
    <tr>
        <th>2</th>
        <th>6</th>
        <th>PHP-BOOK TITLE #2</th>
        <th>AUTHOR #2</th>
        <td><input type="button"value="view"></td>
        <td><input type="button"value="edit"></td>
        <td><input type="button"value="delete"></td>
        <td><input type="button"value="trash"></td>
    </tr>
    <tr>
        <th>3</th>
        <th>7</th>
        <th>PHP-BOOK TITLE #3</th>
        <th>AUTHOR#3</th>
        <td><input type="button"value="view"></td>
        <td><input type="button"value="edit"></td>
        <td><input type="button"value="delete"></td>
        <td><input type="button"value="trash"></td>
    </tr>
    <tr>
        <td colspan="8">PAGE : <1,2,3,4,5,6,7></td>
    </tr>
    <tr>
        <td colspan="9"><input type="button"value="Add New Book Title">
            <input type="button"value="View Trash Name">
            <input type="button"value="Download As PDF">
            <input type="button"value="Download As EXCEL File">
        </td>
    </tr>
</table>

</body>
</html>