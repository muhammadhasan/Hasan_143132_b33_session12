
<!DOCTYPE html>
<html>
<title>
html tags
</title>
<body>
<!HTML Heading Tags (h1-h6)>
<h1>this is heading</h1>
<h2>this is heading</h2>
<h3>this is heading</h3>
<h4>this is heading</h4>
<h5>this is heading</h5>
<h6>this is heading</h6>




<!small tag>
<p>this is large but <small>this text is small</small></p>

<!p tag>
<p> this is another program . </p>

<!marked text>
<p>you can use mark tag to <mark>highlight</mark> the text.</p>

<!deleted text>
<p></p><del>this line is treated as deleted .</del></p>

<!underlined text>
<p><u> this text is underlined</u></p>

<!b tag>
<p>this snippet of text is <b>rendered as bold text</b> </p>

<!italics i tag>
<p>this snippet of text is <i>rendered as bold text</i></p>

<!blockquotes>
<p><blockquotes>this is a long text .this is a long text .this is a long text .
this is a long text .this is a long text .this is a long text .
this is a long text .this is a long text .</blockquotes></p>

<!abbr tag>
<p>the <abbr title="World Health Organization">WHO</abbr> is founded in 1948</p>

<!acronym>
<p> do the work <acronym title ="As soon as possible">ASAP</acronym></p>


<!address>
<address>
created by pondit.com<br>
    <a> href= </a>
</address>
</body>
</html>


